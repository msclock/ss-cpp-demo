# SS CPP Demo

SS CPP Demo

[![Documentation](https://img.shields.io/badge/Documentation-sphinx-blue)](https://msclock.gitlab.io/ss-cpp-demo)
[![License](https://img.shields.io/gitlab/license/msclock/ss-cpp-demo?gitlab_url=https%3A%2F%2Fgitlab.com)](https://gitlab.com/msclock/ss-cpp-demo/-/blob/master/LICENSE)
[![SS Cpp](https://img.shields.io/badge/Serious%20Scaffold-c++-blue)](https://github.com/serious-scaffold/ss-cpp)

[![CI](https://gitlab.com/msclock/ss-cpp-demo/badges/master/pipeline.svg)](https://gitlab.com/msclock/ss-cpp-demo/-/commits/master)
[![codecov](https://codecov.io/gh/msclock/ss-cpp-demo/branch/master/graph/badge.svg?token=123456789)](https://codecov.io/gh/msclock/ss-cpp-demo)
[![Release](https://gitlab.com/msclock/ss-cpp-demo/-/badges/release.svg)](https://gitlab.com/msclock/ss-cpp-demo/-/releases)

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![clang-format](https://img.shields.io/badge/clang--format-enabled-blue)](https://github.com/pre-commit/mirrors-clang-format)
[![cmake-format](https://img.shields.io/badge/cmake--format-enabled-blue)](https://github.com/cheshirekow/cmake-format-precommit)
[![codespell](https://img.shields.io/badge/codespell-enabled-blue)](https://github.com/codespell-project/codespell)
[![markdownlint](https://img.shields.io/badge/markdownlint-enabled-blue)](https://github.com/igorshubovych/markdownlint-cli)
[![shellcheck](https://img.shields.io/badge/shellcheck-enabled-blue)](https://github.com/shellcheck-py/shellcheck-py)

<!-- writes more things here -->

## License

MIT License, for more details, see the [LICENSE](https://gitlab.com/msclock/ss-cpp-demo/-/blob/master/LICENSE) file.
